# Lithp

![Lithp Logo](./img/icon_rgb_small.png)

Lithp is a Lisp-inspired programming language developed by me (Earthnuker) in my spare time to learn about lexer-,parser- and interpreter-design

## Installation

- Install a nightly rust compiler: `rustup install nightly`
- Run the REPL `cargo run --release --bin lithp`

## Syntax

As stated above, the syntax is lisp-inspired so to add two number you would use `(+ 1 2)` which returns `3`

To build a list of values you can use curly braces like so `(= x [1,2,3])` , commas are optional so `(= y [1 2 3])` is valid too.

To append to a list you use the `+` operator like this `(= x []);(= x (+ x 1 2 3 4))` which sets the value of `x` to a list containing `1,2,3,4`

## Data Types

Available data types are:

- Bool: either true (`#t`) or fals (`#f`)
- Num: any 64-bit floating point number
- None: nothing
- Err: an error message in the form of a string
- Sym: a symbolic expression that can be evaluated later
- List: a list containing 0 or more of any data type

## Functions

Functions can be defined in one of two ways:

### Anonymous functions

Anonymous functions are functions that do not have to be bound to a name, they do not support named arguments

Example:

```lisp
(= f '(+ %0 %1))
(f 1 2)
```

### Named function

Named functions are function that have a name (duh!), their parameters can also be named

Example:

```lisp
(fn (t [x y z] (+ %x %y %z)))
(t 1 2 3)
```
## Symbolic Expression

Symbolic expressions are expression that can be evaluated at a later time

### Examples:

```lisp
(= e '(+ 1 %0))
(eval e 1)
```

```lisp
(= e '(+ 1 1))
(e)
```

## Built-in functions:

- `push`: Pushes one or more values onto the value-stack
    - Example: `(push 1);(push 1 2 3)`
- `pop`: Pops a value from the value stack and returns it, returns an error if the stack is empty
    - Example: (= x (pop))
- `eval`: evaluates a symbolic expression, with optional arguments
    - Example: `(= e '(+ 1 %0));(eval e 1)`
- `unset`: Removes a variable from the current execution context, can also be used to undefine functions
    - Example: `(unset x)`
- `info`: prints information about the current execution context
    - Example: `(info)`
- `reset`: clears the current execution context
    - Example: `(reset)`
- `fn`: defines a function
    - Example: `(fn (t [x y z] (+ %x %y %z)))`
- `sym`: returns a symbolic expressions containing its arguments, if more than one argument is passed it returns a list
    - Example: `(sym (+ 1 2) x)`
- `str`: return the string representation of its arguments
    - Example: `(+ (str (+ [] 1 2 3)) "|" (str (+ [] 4 5 6)))`
- `dump`: returns a string containing the debug-string representation of its arguments
    - Example: `(dump (+ 1 2 3) (+ 4 5 6))`
- `version`: prints version info
    - Example: `(version)`
- `var`: returns the value of a variable
    - Example: `(var x)`
- `val`: returns the value of an expression
    - Example: `(val (+ 1 2))`
- `print`: prints its arguments to the console
    - Example: `(print 1 2 3)`
- `exit`: exits the interpreter
    - Example: `(exit)`