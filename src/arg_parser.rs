use std::path::PathBuf;

#[derive(Debug, StructOpt)]
pub struct Opt {
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    pub verbose: u64,

    #[structopt(short = "t", long = "test", help = "Run tests")]
    pub do_test: bool,

    #[structopt(short = "m", long = "vm_test", help = "Run VM test")]
    pub do_vm_test: bool,

    #[structopt(short = "o", parse(from_os_str))]
    pub outfile: Option<PathBuf>,

    #[structopt(parse(from_os_str))]
    pub infile: Option<PathBuf>,
}
