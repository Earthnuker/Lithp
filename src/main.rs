extern crate lithp;
extern crate loggerv;
#[macro_use]
extern crate log;
extern crate colored;
extern crate structopt;
use colored::Colorize;
use lithp::{
    arg_parser::Opt,
    build_ast,
    compiler,
    eval::Context,
    logger,
    repl,
    version,
    vm,
};
use structopt::StructOpt;

fn banner() {
    let banner = vec![
        (
            "██████████",
            "██████████",
            "██",
        ),
        ("██      ██", "██      ██", "██"),
        ("██  ██  ██", "██  ██  ██", "██"),
        ("██  ██  ██", "██  ██  ██", ""),
        (
            "██  ██  ██",
            "██  ██  ██████",
            "",
        ),
        ("██  ██  ██", "██  ██      ██", ""),
        (
            "██  ██  ██",
            "██  ██████  ██",
            "",
        ),
        ("██  ██  ██", "██          ██", ""),
        (
            "██  ██  ██",
            "██████████████",
            "",
        ),
        ("██  ██  ██", "", ""),
        (
            "██  ██  ██████████████████",
            "",
            "",
        ),
        ("██  ██                  ██", "", ""),
        (
            "██  ██████████████████  ██",
            "",
            "",
        ),
        ("██                      ██", "", ""),
        (
            "██████████████████████████",
            "",
            "",
        ),
    ];
    println!();
    for (r, g, b) in banner {
        println!("  {}  {}  {}", r.red(), g.green(), b.blue());
    }
    println!();
    version();
    println!();
}

fn test(ctx: &Context, args: &Opt) {
    let mut ctx = ctx.clone();
    let inp = vec![
        /*
        "(= x 2);(= y 3);(= z 12)",
        "(+ (* 1 2) (* 3 4) 5)",
        "(+ (* 1 x) (* 3 y) z)",
        "(+ (* 1 x) (* 3 y) z)",
        "(= x (* 1 x))",
        "(= x '(* %0 %1))",
        "(x 1 2)",
        "(= x '(* %0 %3))",
        "(x 1 2 3 4)",
        "(fn (test [x y z] (+ %x %y %z)))",
        "(test 1 2 3)",
        "(= l [1, [1,2,3,4],3])",
        "(= l [1, '(+ %0 %1),#t])",
        "(= x -2)",
        "(l[x y] 1 2)",
        "(= x '(print \"hello world!\"))",
        "x;(x)",
        "(x;(x))",
        //"(+ 1 2)",
        "(exit)",
        /*
        "(= f '(+ %0 %1));(f 1 2)",
        //"(= fib '(? (< %0 2) %0 (+ (fib (+ %0 -1)) (fib (+ %0 -2)))));(= i 0);(@ (< i 15) (= ret (fib i)) (print ret) (= i (+ 1 i)) ret)",
        "exit",
        */
        */
        "(= fib '(? (< %0 2) %0 (+ (fib (+ %0 -1)) (fib (+ %0 -2))))),(= i 0),(@ (< i 15) (= ret (fib i)) (print ret) (= i (+ 1 i)) ret)",
    ];
    for blck in inp {
        println!("lithp::test> {}", blck);
        match build_ast(&blck) {
            Ok(ast) => {
                println!("Result: {:?}", ast.eval(&mut ctx));
                ctx.print();
            },
            Err(msg) => error!("{}", msg),
        };
    }
}

fn main() {
    let args = Opt::from_args();
    banner();
    logger::init(&args);
    if args.do_vm_test {
        vm::test();
        return;
    }
    let mut ctx = Context::new();
    if args.do_test {
        test(&ctx, &args);
    }
    repl(&mut ctx);
}
