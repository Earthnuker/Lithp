use arg_parser::Opt;
use loggerv;
pub fn init(args: &Opt) {
    loggerv::Logger::new()
        .verbosity(args.verbose)
        .level(true)
        .line_numbers(true)
        .module_path(true)
        .colors(true)
        .separator(": ")
        .init()
        .unwrap();
}
