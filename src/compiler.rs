use build_ast;
use eval::DataType as DT;
use parser::{
    Op,
    AST,
};
use vm::Inst;

fn sym_list(ast: &AST) -> Vec<String> {
    use AST::*;
    let mut names: Vec<String> = Vec::new();
    match ast {
        Sym(node) => {
            for sym in sym_list(&node) {
                names.push("Sym#".to_owned() + &sym.to_string());
            }
        },
        Chain(nodes) | List(nodes) => {
            for node in nodes {
                for sym in sym_list(&node) {
                    names.push(format!("Lst#{}", sym));
                }
            }
        },
        Index(lst, idx) => {
            for sym in sym_list(&lst) {
                names.push(format!("Idx_Val#{}", sym));
            }
            for sym in sym_list(&idx) {
                names.push(format!("Idx_Idx#{}", sym));
            }
        },
        Call(func, args) => {
            for sym in sym_list(&func) {
                names.push(format!("Call_Func#{}", sym));
            }
            for arg in args {
                for sym in sym_list(arg) {
                    names.push(format!("Call_Arg#{}", sym));
                }
            }
        },
        GlobalIdent(name) => names.push(format!("Global#{}", name)),
        Ident(name) => names.push(format!("Ident#{}", name)),
        Arg(n) => names.push(format!("Arg#{}", n)),
        NamedArg(name) => names.push(format!("NArg#{}", name)),
        Val(DT::Sym(node)) => {
            for sym in sym_list(node) {
                names.push(format!("Val_Sym#{}", sym));
            }
        },
        Val(DT::List(nodes)) => {
            for node in nodes {
                for sym in sym_list(&Val(node.clone())) {
                    names.push(format!("Val_Lst#{}", sym));
                }
            }
        },
        PtrIdent(name) => {
            names.push(format!("Ptr_Val#{}", name));
        },
        PtrExpr(node) => {
            for sym in sym_list(&node) {
                names.push(format!("Ptr_Exp#{}", sym));
            }
        },
        Ptr(..) | Op(..) | Val(..) | Null => (),
    }
    names.sort();
    names.dedup_by_key(|n| n.to_owned());
    names
}

pub fn compile(tree: &AST) -> Vec<Inst> {
    let mut ret = Vec::new();
    println!("Syms: {:?}", sym_list(&tree));
    ret
}
