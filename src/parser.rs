use eval::DataType as DT;
use lexer::Token;
use std::collections::VecDeque;
#[allow(dead_code)]
#[derive(Debug, Clone, PartialEq)]
pub enum Op {
    Add,
    Sub,
    Mul,
    Div,
    Pow,

    Set,
    If,
    Loop,

    Eq,
    NEq,
    Gt,
    Ge,
    Lt,
    Le,

    And,
    Or,
    Xor,
}

#[allow(dead_code)]
#[derive(Debug, Clone, PartialEq)]
pub enum AST {
    Call(Box<AST>, VecDeque<AST>),
    Index(Box<AST>, Box<AST>),
    Op(Op),
    List(VecDeque<AST>),
    Chain(VecDeque<AST>),
    Sym(Box<AST>),
    Ident(String),
    PtrIdent(String),
    GlobalIdent(String),
    Arg(usize),
    Ptr(u64),
    PtrExpr(Box<AST>),
    Val(DT),
    NamedArg(String),
    Null,
}
fn get_op(sym: &str) -> AST {
    let ret = match sym {
        "+" => Op::Add,
        "-" => Op::Sub,
        "*" => Op::Mul,
        "/" => Op::Div,
        "**" => Op::Pow,
        "=" => Op::Set,
        "==" => Op::Eq,
        "!=" => Op::NEq,
        ">" => Op::Gt,
        ">=" => Op::Ge,
        "<" => Op::Lt,
        "<=" => Op::Le,
        "?" => Op::If,
        "&" => Op::And,
        "|" => Op::Or,
        "^" => Op::Xor,
        "@" => Op::Loop,
        name => return AST::Ident(name.to_string()),
    };
    AST::Op(ret)
}

fn make_chain(nodes: &VecDeque<AST>) -> AST {
    let mut nodes = nodes.clone();
    if nodes.len() > 1 {
        AST::Chain(nodes)
    } else {
        nodes.pop_front().unwrap_or(AST::Null)
    }
}

fn make_call(tokens: &VecDeque<Token>) -> Result<AST, String> {
    let mut nodes = VecDeque::new();
    for tok in tokens {
        nodes.push_back(to_ast_node(&tok)?);
    }
    let op = nodes.pop_front().unwrap();
    Ok(AST::Call(Box::new(op), nodes))
}

pub fn to_ast(tokens: &VecDeque<Token>) -> Result<AST, String> {
    let mut ret: VecDeque<AST> = VecDeque::new();
    for tok in tokens {
        ret.push_back(to_ast_node(&tok)?);
    }
    Ok(make_chain(&ret))
}

fn to_ast_node(token: &Token) -> Result<AST, String> {
    let node = match token {
        Token::Expr(e) => make_call(e)?,
        Token::SymExpr(e) => AST::Sym(Box::new(make_call(e)?)),
        Token::MemExpr(e) => AST::PtrExpr(Box::new(make_call(e)?)),
        Token::IndexExpr(val, idx) => {
            let val = to_ast_node(val)?;
            let idx = to_ast_node(idx)?;
            AST::Index(Box::new(val), Box::new(idx))
        },
        Token::Num(n) => AST::Val(DT::Num(*n)),
        Token::Bool(b) => AST::Val(DT::Bool(*b)),
        Token::Str(s) => AST::Val(DT::Str(s.clone())),
        Token::List(l) => AST::List({
            let mut v = VecDeque::new();
            for val in l {
                v.push_back(to_ast_node(val)?);
            }
            v
        }),
        Token::Ident(name) => AST::Ident(name.clone()),
        Token::GlobalIdent(name) => AST::GlobalIdent(name.clone()),
        Token::Sym(name) => AST::Sym(Box::new(AST::Ident(name.clone()))),
        Token::Op(op) => get_op(op),
        Token::Arg(a) => AST::Arg(*a),
        Token::NamedArg(a) => AST::NamedArg(a.clone()),
        Token::Null => AST::Null,
        Token::Addr(n) => AST::Ptr(*n),
        Token::MemIdent(n) => AST::PtrIdent(n.clone()),
        _ => return Err(format!("Found {:?}, this should not happen!", token)),
    };
    Ok(node)
}
