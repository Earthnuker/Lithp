use self::DataType as DT;
use parser::{
    Op,
    AST,
};
use std::{
    collections::{
        HashMap,
        VecDeque,
    },
    fmt,
    ops::Try,
    process,
};

#[derive(Debug, Clone, PartialEq)]
pub enum DataType {
    Err(String),
    Num(f64),
    Bool(bool),
    Ptr(u64),
    Sym(Box<AST>),
    List(Vec<DT>),
    Str(String),
    None,
}

impl Try for DataType {
    type Error = String;
    type Ok = DataType;

    fn into_result(self) -> Result<DT, String> {
        match self {
            DT::Err(msg) => Err(msg),
            other => Ok(other),
        }
    }

    fn from_ok(v_ok: DT) -> DT {
        v_ok
    }

    fn from_error(v_err: String) -> DT {
        DT::Err(v_err)
    }
}

impl fmt::Display for DataType {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            DT::Num(num) => format!("{}", num),
            DT::Sym(tree) => format!("{:?}", tree),
            DT::List(vals) => {
                let mut s = String::new();
                s += "[";
                let mut first = true;
                for val in vals {
                    if !first {
                        s += ",";
                    }
                    s += &format!("{}", val);
                    first = false;
                }
                s += "]";
                s
            },
            DT::Bool(val) => format!("{}", val),
            DT::Ptr(ptr) => format!("@{:016x}", ptr),
            DT::Str(val) => val.clone(),
            DT::Err(msg) => format!("Error(\"{}\")", msg),
            DT::None => String::from("<None>"),
        };
        fmt.write_str(&s)?;
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum FuncVal {
    Anonymous(Box<AST>),
    Named(FuncDef),
}

#[derive(Debug, Clone, PartialEq)]
pub struct FuncDef {
    pub args: VecDeque<String>,
    pub body: AST,
}

#[derive(Debug, Clone, PartialEq, Default)]
pub struct Context {
    stack:      Vec<DT>,
    call_stack: Vec<Context>,
    env:        HashMap<String, DT>,
    named_args: HashMap<String, DT>,
    args:       VecDeque<DT>,
    funcs:      HashMap<String, FuncDef>,
}

impl Context {
    pub fn new() -> Context {
        Default::default()
    }

    pub fn clear(&mut self) {
        self.stack.clear();
        self.args.clear();
        self.env.clear();
        self.named_args.clear();
        self.funcs.clear();
        self.call_stack.clear();
    }

    pub fn print(&self) {
        if !&self.env.is_empty() {
            println!("Env:");
            for (k, v) in &self.env {
                println!("\t{}: {:?}", k, v);
            }
        };
        if !&self.named_args.is_empty() {
            println!("Named Args:");
            for (k, v) in &self.named_args {
                println!("\t{}: {:?}", k, v);
            }
        };
        if !&self.funcs.is_empty() {
            println!("Funcs:");
            for (k, v) in &self.funcs {
                println!("\t{}:", k);
                println!("\t\tArgs: {:?}", v.args);
                println!("\t\tBody: {:?}", v.body);
            }
        };
        if !&self.stack.is_empty() {
            println!("Stack:");
            for val in &self.stack {
                println!("\t{:?}", val);
            }
        }
        // if !&self.call_stack.is_empty() {
        // println!("Call Stack: {:?}", &self.call_stack);
        // }
        //
    }

    pub fn get_var(&self, var: &str) -> (bool, DT) {
        match self.named_args.get(var) {
            Some(val) => (true, val.clone()),
            None => match self.env.get(var) {
                Some(val) => (true, val.clone()),
                None => (false, DT::Err(format!("undefined variable: {:?}", var))),
            },
        }
    }

    pub fn get_global(&self, var: &str) -> DT {
        if let (true, val) = self.get_var(var) {
            return val;
        }
        for frame in self.call_stack.clone() {
            if let (true, val) = frame.get_var(var) {
                return val;
            }
        }
        DT::Err(format!("undefined variable: {:?}", var))
    }

    pub fn get_arg(&self, var: &str) -> DT {
        match self.named_args.get(var) {
            Some(val) => val.clone(),
            None => DT::Err(format!("undefined argument: {:?}", var)),
        }
    }

    pub fn get_func(&self, func: &str) -> Result<FuncVal, String> {
        match self.get_global(func) {
            DT::Sym(ast) => Ok(FuncVal::Anonymous(Box::new((*ast).clone()))),
            _ => match self.funcs.get(func) {
                Some(func_def) => Ok(FuncVal::Named((*func_def).clone())),
                None => Err(format!("undefined function: {:?}", func)),
            },
        }
    }
}

fn fold_op(
    _op: &Op,
    base: f64,
    func: &Fn(f64, f64) -> f64,
    args: &VecDeque<AST>,
    ctx: &mut Context,
) -> DT {
    let mut ret = base;
    let mut first = true;
    for val in args {
        let val = val.eval(ctx)?;
        match val {
            DT::Num(num) => {
                ret = func(ret, num);
            },
            DT::Bool(val) => {
                ret = func(ret, f64::from(val as u8));
            },
            DT::Str(ref s) if (*_op == Op::Add) && first => {
                let mut ret = s.clone();
                let mut args = args.clone();
                args.pop_front();
                while let Some(v) = args.pop_front() {
                    ret += &format!("{}", v.eval(ctx)?);
                }
                return DT::Str(ret);
            },
            DT::List(ref l) if (*_op == Op::Add) && first => {
                let mut vals = l.clone();
                let mut args = args.clone();
                args.pop_front();
                while let Some(v) = args.pop_front() {
                    let v = v.eval(ctx)?;
                    vals.push(v);
                }
                return DT::List(vals);
            },
            other => return DT::Err(format!("Invalid argument: {:?}", other)),
        }
        first = false;
    }
    DT::Num(ret)
}

fn fold_bool_op(
    _op: &Op,
    func: &Fn(bool, bool) -> bool,
    args: &VecDeque<AST>,
    ctx: &mut Context,
) -> DT {
    let mut args = args.clone();
    let mut ret = match args.pop_front().unwrap_or(AST::Null).eval(ctx)? {
        DT::Bool(val) => val,
        DT::Num(num) => num != 0.0,
        DT::List(vals) => !vals.is_empty(),
        other => return DT::Err(format!("Invalid argument: {:?}", other)),
    };
    for val in args {
        match &val.eval(ctx)? {
            DT::Bool(val) => ret = func(ret, *val),
            DT::Num(num) => ret = func(ret, *num != 0f64),
            DT::List(vals) => ret = func(ret, !vals.is_empty()),
            DT::None => (),
            other => return DT::Err(format!("Invalid argument: {:?}", other)),
        }
    }
    DT::Bool(ret)
}

fn fold_cmp_op(
    _op: &Op,
    func: &Fn(f64, f64) -> bool,
    args: &VecDeque<AST>,
    ctx: &mut Context,
) -> DT {
    let mut ret = true;
    let mut args = args.clone();
    let res = args.pop_front().unwrap_or(AST::Null);
    let res_ev = res.eval(ctx)?;
    let prev = match &res_ev {
        DT::Num(num) => num,
        other => return DT::Err(format!("Invalid argument: {:?}", other)),
    };
    for val in args {
        match val.eval(ctx)? {
            DT::Num(num) => {
                ret &= func(*prev, num);
            },
            DT::None => {},
            other => return DT::Err(format!("Invalid argument: {:?}", other)),
        }
    }
    DT::Bool(ret)
}

fn eval_named_func(func: &FuncDef, args: &VecDeque<AST>, ctx: &mut Context) -> DT {
    if func.args.len() != args.len() {
        return DT::Err(format!(
            "Invalid number of arguments: expected {} but got {}",
            func.args.len(),
            args.len(),
        ));
    }
    let o_ctx = ctx.clone();
    let o_nargs = ctx.named_args.clone();
    ctx.call_stack.push(o_ctx);
    for (var, val) in func.args.iter().zip(args.iter()) {
        let res = val.eval(ctx);
        ctx.named_args.insert(var.to_string(), res);
    }
    let ret = func.body.eval(ctx);
    ctx.named_args = o_nargs;
    let mut o_env = ctx.call_stack.pop().unwrap().env;
    o_env.extend(ctx.clone().env);
    ctx.env = o_env;
    ret
}

fn eval_func(tree: &AST, args: &VecDeque<AST>, ctx: &mut Context) -> DT {
    let o_ctx = ctx.clone();
    ctx.call_stack.push(o_ctx);
    let mut ev_args = VecDeque::new();
    for arg in args {
        ev_args.push_back(arg.eval(ctx)?)
    }
    ctx.args = ev_args;
    let ret = tree.eval(ctx)?;
    let frame = ctx.call_stack.pop().unwrap();
    ctx.args = frame.args;
    ctx.funcs = frame.funcs;
    ctx.named_args = frame.named_args;
    ret
}

fn to_arg_list(nodes: &VecDeque<AST>) -> Result<VecDeque<String>, String> {
    let mut nodes = nodes.clone();
    let mut ret = VecDeque::new();
    while let Some(node) = nodes.pop_front() {
        match node {
            AST::Ident(name) => ret.push_back(name),
            other => return Err(format!("Invalid arguent: {:?}", other)),
        }
    }
    Ok(ret)
}

fn eval_call_var(func: &str, args: &VecDeque<AST>, ctx: &mut Context) -> DT {
    let mut args = args.clone();
    match func {
        "push" => {
            for arg in args {
                match arg.eval(ctx)? {
                    DT::None => {},
                    other => ctx.stack.push(other),
                }
            }
            DT::None
        },
        "pop" => match ctx.stack.pop() {
            Some(val) => val,
            None => DT::Err("pop from empty stack".to_string()),
        },
        "eval" => {
            let expr = match args.pop_front() {
                Some(val) => val.eval(ctx)?,
                None => DT::None,
            };
            match expr {
                DT::Sym(tree) => eval_func(&tree, &args, ctx),
                other => DT::Err(format!("Invalid expression: {:?}", other)),
            }
        },
        "unset" => {
            for arg in args {
                match arg {
                    AST::Ident(name) => {
                        ctx.env.remove(&name);
                        ctx.funcs.remove(&name);
                    },
                    other => return DT::Err(format!("Invalid argument: {:?}", other)),
                }
            }
            DT::None
        },
        "info" => {
            println!("==========");
            ctx.print();
            DT::None
        },
        "dump" => DT::Str(format!("{:?}", args)),
        "version" => {
            use version;
            version();
            DT::None
        },
        "reset" => {
            ctx.clear();
            println!("Environment cleared");
            DT::None
        },
        "fn" => {
            match args.pop_front().unwrap() {
                AST::Call(box func, rest) => match func {
                    AST::Ident(name) => {
                        let mut rest = rest.clone();
                        let func_args = match rest.pop_front() {
                            Some(AST::List(args)) => to_arg_list(&args)?,
                            None => VecDeque::new(),
                            Some(other) => {
                                return DT::Err(
                                    format!("Invalid function definition: {:?}", other).to_string(),
                                )
                            },
                        };
                        ctx.funcs.insert(
                            name,
                            FuncDef {
                                args: func_args,
                                body: AST::Chain(rest),
                            },
                        );
                    },
                    other => {
                        return DT::Err(
                            format!("Invalid function operator: {:?}", other).to_string(),
                        )
                    },
                },
                other => {
                    return DT::Err(format!("Invalid function operator: {:?}", other).to_string())
                },
            };
            DT::None
        },
        "sym" => {
            let mut ret = Vec::new();
            for arg in args {
                ret.push(DT::Sym(Box::new(arg)));
            }
            if ret.len() < 2 {
                ret.pop().unwrap_or(DT::None)
            } else {
                DT::List(ret)
            }
        },
        "str" => {
            let mut ret = Vec::new();
            for arg in args {
                ret.push(arg.eval(ctx)?);
            }
            let mut res;
            if ret.len() < 2 {
                res = ret.pop().unwrap_or(DT::None)
            } else {
                res = DT::List(ret)
            }
            DT::Str(format!("{}", res))
        },
        "val" => {
            let mut ret = Vec::new();
            for arg in args {
                ret.push(arg.eval(ctx)?);
            }
            if ret.len() < 2 {
                ret.pop().unwrap_or(DT::None)
            } else {
                DT::List(ret)
            }
        },
        "var" => {
            let mut ret = Vec::new();
            for arg in args {
                let arg = match arg {
                    AST::Ident(name) => ctx.get_global(&name),
                    other => DT::Err(format!("invalid variable name: {:?}", other)),
                };
                ret.push(arg);
            }
            if ret.len() < 2 {
                ret.pop().unwrap_or(DT::None)
            } else {
                DT::List(ret)
            }
        },
        "print" => {
            for arg in args {
                print!("{}", arg.eval(ctx)?);
            }
            println!();
            DT::None
        },
        "exit" => process::exit(0),
        other => {
            use self::FuncVal::{
                Anonymous,
                Named,
            };
            match ctx.get_func(other) {
                Ok(func) => match func {
                    Named(func_def) => eval_named_func(&func_def, &args, ctx),
                    Anonymous(body) => eval_func(&body, &args, ctx),
                },
                Err(msg) => DT::Err(msg),
            }
        },
    }
}

fn eval_call(func: &Op, args: &VecDeque<AST>, ctx: &mut Context) -> DT {
    let mut args = args.clone();
    match func {
        Op::Add => fold_op(&func, 0f64, &|acc, v| acc + v, &args, ctx),

        Op::Sub => fold_op(&func, 0f64, &|acc, v| acc - v, &args, ctx),

        Op::Mul => fold_op(&func, 1f64, &|acc, v| acc * v, &args, ctx),
        Op::Div => fold_op(&func, 1f64, &|acc, v| acc / v, &args, ctx),
        Op::Pow => fold_op(&func, 1f64, &|acc, v| v.powf(acc), &args, ctx),
        Op::Eq => fold_cmp_op(&func, &|a, b| (a - b).abs() < 1e-16, &args, ctx),
        Op::NEq => fold_cmp_op(&func, &|a, b| (a - b).abs() > 1e-16, &args, ctx),
        Op::Gt => fold_cmp_op(&func, &|a, b| a > b, &args, ctx),
        Op::Lt => fold_cmp_op(&func, &|a, b| a < b, &args, ctx),
        Op::Ge => fold_cmp_op(&func, &|a, b| a >= b, &args, ctx),
        Op::Le => fold_cmp_op(&func, &|a, b| a <= b, &args, ctx),
        Op::And => fold_bool_op(&func, &|a, b| a && b, &args, ctx),
        Op::Or => fold_bool_op(&func, &|a, b| a || b, &args, ctx),
        Op::Xor => fold_bool_op(&func, &|a, b| a ^ b, &args, ctx),
        Op::Loop => {
            let cond = args.pop_front().unwrap_or(AST::Null);
            let body = AST::Chain(args);
            let mut ret = DT::None;
            loop {
                match cond.eval(ctx)? {
                    DT::Bool(true) => {
                        ret = body.eval(ctx)?;
                    },
                    DT::Bool(false) => break,
                    other => return DT::Err(format!("Invalid comparison result: {:?}", other)),
                }
            }
            ret
        },
        Op::If => {
            if args.len() < 2 || args.len() > 3 {
                return DT::Err("if block expects 2 or 3 arguments".to_string());
            }
            let mut cond;
            let mut true_body;
            let mut false_body;
            cond = args.pop_front().unwrap_or(AST::Null);
            true_body = args.pop_front().unwrap_or(AST::Null);
            false_body = args.pop_front().unwrap_or(AST::Null);
            let cond_res = match cond.eval(ctx)? {
                DT::Bool(ret) => ret,
                other => return DT::Err(format!("Invalid comparison result: {:?}", other)),
            };
            (if cond_res { &true_body } else { &false_body }).eval(ctx)?
        },
        Op::Set => match args.pop_front() {
            Some(AST::Ident(var)) => {
                let mut ret = DT::None;
                while let Some(val) = args.pop_front() {
                    let val = val.eval(ctx)?;
                    ctx.env.insert(var.clone(), val.clone());
                    ret = val.clone();
                }
                ret
            },
            Some(other) => DT::Err(format!("Can't assign to {:?}!", other)),
            None => DT::Err("An assignment target is required!".to_string()),
        },
    }
}

impl AST {
    pub fn eval(&self, ctx: &mut Context) -> DT {
        // println!("Eval {:?} in", ast);
        // ctx.print();
        let result = match self.clone() {
            AST::Chain(nodes) => {
                let mut ret = DT::None;
                for node in nodes {
                    ret = node.eval(ctx)?;
                }
                ret
            },
            AST::Ident(var) => ctx.get_var(&var).1,
            AST::GlobalIdent(var) => ctx.get_global(&var),
            AST::Val(v) => v,
            AST::List(l) => {
                let mut ret = Vec::new();
                for v in l {
                    ret.push(v.eval(ctx))
                }
                DT::List(ret)
            },
            AST::Arg(n) => match ctx.args.clone().get(n) {
                Some(val) => (*val).clone(),
                None => DT::Err(format!("Failed to retrieve function argument %{}", n)),
            },
            AST::Call(box func, mut args) => match func {
                AST::Op(op) => eval_call(&op, &args, ctx),
                AST::Ident(name) => eval_call_var(&name, &args, ctx),
                other => match other.eval(ctx)? {
                    DT::Sym(box body) => eval_func(&body, &args, ctx)?,
                    other => DT::Err(format!("{:?} is not callable", other)),
                },
            },
            AST::Null => DT::None,
            AST::Sym(tree) => DT::Sym(tree),
            AST::NamedArg(name) => ctx.get_arg(&name),
            AST::Index(val, idx) => {
                let o_idx = match idx.eval(ctx)? {
                    DT::Num(n) => n as i64,
                    other => return DT::Err(format!("Invalid index: {:?}", other)),
                };
                let from_end = o_idx < 0;
                let mut idx = o_idx.abs() as usize;
                let val = val.eval(ctx)?;
                match val {
                    DT::List(v) => {
                        if from_end {
                            idx = v.len() - idx;
                        };
                        match v.get(idx) {
                            Some(val) => (*val).clone(),
                            None => DT::Err(format!("Index out of range: {}", o_idx)),
                        }
                    },
                    DT::Str(s) => {
                        if from_end {
                            idx = s.len() - idx;
                        };
                        let mut res = DT::Err(format!("Index out of range: {}", o_idx));

                        for (i, c) in s.chars().enumerate() {
                            if i == idx {
                                res = DT::Str(c.to_string());
                            }
                        }
                        res
                    },
                    other => DT::Err(format!("Can't index {:?}", other)),
                }
            },
            other => DT::Err(format!("can't evaluate {:?}", other)),
        };
        trace!("{:?} => {:?}", self, result);
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use lexer::{
        to_expr,
        tokenize,
    };
    use parser::to_ast;
    #[allow(dead_code)]
    fn build_ast(inp: &str, ctx: &Context) -> Result<AST, String> {
        let tokens = tokenize(&inp)?;
        let nested_tokens = to_expr(&mut tokens.clone())?;
        let ast = to_ast(&nested_tokens)?;
        Ok(ast)
    }

    #[allow(dead_code)]
    fn test_eval(inp: &str, ctx: &mut Context) -> DT {
        let ast = match build_ast(inp, ctx) {
            Ok(ast) => ast,
            Err(msg) => {
                panic!("{}", msg);
            },
        };
        ast.eval(ctx)
    }

    #[test]
    fn eval_1() {
        let mut ctx = Context::new();
        let ret = test_eval("(+ 1 2)", &mut ctx);
        assert_eq!(ret, DT::Num(3.0));
    }
    #[test]
    fn eval_2() {
        let mut ctx = Context::new();
        let ret = test_eval("(+ (* 8 0.5) (* 2 4))", &mut ctx);
        assert_eq!(ret, DT::Num(12.0));
    }

    #[test]
    fn eval_3() {
        let mut ctx = Context::new();
        for cmd in vec![
            "(= fib '(? (< %0 2) %0 (+ (fib (+ %0 -1)) (fib (+ %0 -2)))))",
            "(= i 0)",
            "(@ (< i 15) (= ret (fib i)) (print ret) (= i (+ 1 i)))",
        ] {
            test_eval(cmd, &mut ctx);
        }
        let ret = ctx.get_var("ret").1;
        ctx.print();
        assert_eq!(ret, DT::Num(377.0));
    }
}
