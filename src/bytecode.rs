use eval::DataType as DT;
use parser::{
    Op,
    AST,
};
#[derive(Debug)]
pub enum Word {
    U8(u8),
    U16(u16),
    U32(u32),
    U64(u64),
}
pub trait Encodable {
    fn get_marker(&self) -> u8;
    fn get_words(&self) -> Vec<Word>;
}
#[derive(Debug)]
pub struct Tagged(u8, Vec<Word>);

impl Tagged {
    pub fn new<T: Encodable>(val: &T) -> Tagged {
        Tagged(val.get_marker(), val.get_words())
    }
}

impl Encodable for Op {
    fn get_marker(&self) -> u8 {
        match self {
            Op::Add => 1,
            Op::Sub => 2,
            Op::Mul => 3,
            Op::Div => 4,
            Op::Pow => 5,
            Op::Set => 6,
            Op::If => 7,
            Op::Loop => 8,
            Op::Eq => 9,
            Op::NEq => 10,
            Op::Gt => 11,
            Op::Ge => 12,
            Op::Lt => 13,
            Op::Le => 14,
            Op::And => 15,
            Op::Or => 16,
            Op::Xor => 17,
        }
    }

    fn get_words(&self) -> Vec<Word> {
        Vec::new()
    }
}

impl Encodable for DT {
    fn get_marker(&self) -> u8 {
        match self {
            DT::None => 0,
            DT::Num(..) => 1,
            DT::Bool(..) => 2,
            DT::Str(..) => 3,
            DT::List(..) => 4,
            DT::Sym(..) => 5,
            DT::Err(..) => 6,
            DT::Ptr(..) => 7,
        }
    }

    fn get_words(&self) -> Vec<Word> {
        use self::Word as W;
        let mut ret = Vec::new();
        match self {
            DT::None => (),
            DT::Num(n) => ret.push(W::U64(n.to_bits())),
            DT::Bool(b) => ret.push(W::U8(*b as u8)),
            DT::Str(s) | DT::Err(s) => {
                ret.push(W::U64(s.len() as u64));
                for v in s.bytes() {
                    ret.push(W::U8(v));
                }
            },
            DT::Sym(e) => {
                ret.append(&mut e.get_words());
            },
            DT::Ptr(a) => ret.push(W::U64(*a)),
            DT::List(v) => {
                ret.push(W::U64(v.len() as u64));
                for val in v {
                    ret.append(&mut val.get_words());
                }
            },
        };
        ret
    }
}

impl Encodable for AST {
    fn get_marker(&self) -> u8 {
        match self {
            AST::Null => 0,
            AST::Call(..) => 1,
            AST::Index(..) => 2,
            AST::Op(..) => 3,
            AST::List(..) => 4,
            AST::Chain(..) => 5,
            AST::Sym(..) => 6,
            AST::Val(..) => 7,
            AST::Arg(..) => 8,
            AST::NamedArg(..) => 9,
            AST::Ident(..) => 10,
            AST::GlobalIdent(..) => 11,
            AST::Ptr(..) => 12,
            AST::PtrIdent(..) => 13,
            AST::PtrExpr(..) => 14,
        }
    }

    fn get_words(&self) -> Vec<Word> {
        use self::Word as W;
        let mut ret = Vec::new();
        match self {
            AST::Null => (),
            AST::List(vals) => {
                ret.push(W::U64(vals.len() as u64));
                for val in vals {
                    ret.append(&mut val.get_words());
                }
            },
            AST::Call(op, args) => {
                ret.append(&mut op.get_words());
                ret.push(W::U64(args.len() as u64));
                for arg in args {
                    ret.append(&mut arg.get_words());
                }
            },
            AST::Index(val, idx) => {
                ret.append(&mut val.get_words());
                ret.append(&mut idx.get_words());
            },
            AST::Op(op) => {
                ret.push(W::U8(op.get_marker()));
            },
            AST::Chain(vals) => {
                for val in vals {
                    ret.append(&mut val.get_words());
                }
            },
            AST::Sym(expr) => {
                ret.append(&mut expr.get_words());
            },
            AST::Arg(n) => {
                ret.push(W::U64(*n as u64));
            },
            AST::Val(v) => {
                ret.append(&mut v.get_words());
            },
            AST::Ptr(v) => {
                ret.push(W::U64(*v as u64));
            },
            AST::PtrExpr(e) => {
                ret.append(&mut e.get_words());
            },
            AST::Ident(s) | AST::GlobalIdent(s) | AST::NamedArg(s) | AST::PtrIdent(s) => {
                ret.push(W::U64(s.len() as u64));
                for v in s.bytes() {
                    ret.push(W::U8(v));
                }
            },
        };
        ret
    }
}
impl AST {
    pub fn get_bytes(&self) -> Vec<u8> {
        println!("DBG: {:?}", Tagged::new(&AST::Val(DT::Num(0.1))));
        Vec::new()
    }
}
