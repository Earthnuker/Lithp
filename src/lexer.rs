extern crate regex;
extern crate test;

use self::regex::Regex;
use colored::Colorize;
use std::{
    cmp::max,
    collections::VecDeque,
};
use Token::*;
#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Expr(VecDeque<Token>),
    SymExpr(VecDeque<Token>),
    MemExpr(VecDeque<Token>),
    IndexExpr(Box<Token>, Box<Token>),
    List(VecDeque<Token>),
    Ident(String),
    GlobalIdent(String),
    Sym(String),
    MemIdent(String),
    Op(String),
    Str(String),
    Num(f64),
    Addr(u64),
    Bool(bool),
    Arg(usize),
    NamedArg(String),
    Index(Box<Token>),
    LParen,
    RParen,
    LBrack,
    RBrack,
    Quote,
    Comma,
    Blank,
    Semi,
    Null,
    Ptr,
}

struct LexRule {
    tokenize: Box<Fn(&str) -> Token>,
    rule:     Regex,
}
impl LexRule {
    fn new(re: &str, fun: Box<Fn(&str) -> Token>) -> LexRule {
        LexRule {
            rule:     Regex::new(re).unwrap(),
            tokenize: fun,
        }
    }
}
pub fn tokenize(inp: &str) -> Result<VecDeque<Token>, String> {
    let mut input = inp
        .replace("\n", " ")
        .replace("\r", " ")
        .replace("\t", " ")
        .to_string();
    while input.find("  ") != None {
        input = input.replace("  ", " ");
    }
    input = input.replace(" )", ")");
    input = input.trim().to_string();
    debug!("Cleaned Input: {}", inp.cyan());
    let mut pos = 0;
    let mut tokens = VecDeque::new();
    let mut tokenizer: Vec<LexRule> = Vec::new();
    tokenizer.push(LexRule::new(
        r#"^%([a-zA-Z_][a-zA-Z0-9_]*)"#,
        Box::new(|s| NamedArg(s.to_string())),
    ));
    tokenizer.push(LexRule::new(
        r#"^@([a-zA-Z_][a-zA-Z0-9_]*)"#,
        Box::new(|s| MemIdent(s.to_string())),
    ));
    tokenizer.push(LexRule::new(
        r#"^:([a-zA-Z_][a-zA-Z0-9_]*)"#,
        Box::new(|s| GlobalIdent(s.to_string())),
    ));
    tokenizer.push(LexRule::new(
        r#"^'([a-zA-Z_][a-zA-Z0-9_]*)"#,
        Box::new(|s| Sym(s.to_string())),
    ));
    tokenizer.push(LexRule::new(
        r#"^$(?:0[xX])?([0-9a-fA-F]+)"#,
        Box::new(|n| Addr(u64::from_str_radix(n, 16).unwrap())),
    ));

    tokenizer.push(LexRule::new(r#"^(\s+)"#, Box::new(|_| Blank)));
    tokenizer.push(LexRule::new(
        r#"^(-?[0-9]+(?:\.[0-9]*)?)"#,
        Box::new(|s| Num(s.parse().expect("Error parsing number"))),
    ));
    tokenizer.push(LexRule::new(r#"^(#t)"#, Box::new(|_| Bool(true))));
    tokenizer.push(LexRule::new(r#"^(#f)"#, Box::new(|_| Bool(false))));
    tokenizer.push(LexRule::new(r#"^(')"#, Box::new(|_| Quote)));
    tokenizer.push(LexRule::new(r#"^(\()"#, Box::new(|_| LParen)));
    tokenizer.push(LexRule::new(r#"^(\))"#, Box::new(|_| RParen)));
    tokenizer.push(LexRule::new(r#"^(\[)"#, Box::new(|_| LBrack)));
    tokenizer.push(LexRule::new(r#"^(\])"#, Box::new(|_| RBrack)));
    tokenizer.push(LexRule::new(r#"^(\$)"#, Box::new(|_| Ptr)));
    tokenizer.push(LexRule::new(r#"^(,)"#, Box::new(|_| Comma)));
    tokenizer.push(LexRule::new(
        r#"^%(\d+)"#,
        Box::new(|s| Arg(s.parse().unwrap())),
    ));
    tokenizer.push(LexRule::new(
        r#"^([a-zA-Z_][a-zA-Z0-9_]*)"#,
        Box::new(|s| Ident(s.to_string())),
    ));
    tokenizer.push(LexRule::new(
        r#"^"((?:\\.|[^\\"])*)""#,
        Box::new(|s| Str(s.to_string())),
    ));
    tokenizer.push(LexRule::new(
        r#"^([[:punct:]]+)"#,
        Box::new(|s| Op(s.to_string())),
    ));
    let mut matched;
    while !input.is_empty() {
        matched = false;
        for lex_rule in &tokenizer {
            let mut last_cap = 0;
            for cap in lex_rule.rule.captures_iter(&input) {
                last_cap = max(
                    last_cap,
                    cap.get(0).expect("Error getting capture group").end(),
                );
                for i in 1..cap.len() {
                    matched = true;
                    let m_val = cap.get(i);
                    let s = m_val.map_or("", |m| m.as_str());
                    let tok = m_val.map_or(Null, |m| (lex_rule.tokenize)(m.as_str()));
                    trace!(
                        "{} : {} => {}",
                        format!("{:?}", lex_rule.rule).blue(),
                        s.red(),
                        format!("{:?}", tok).green()
                    );
                    tokens.push_back(tok);
                }
            }
            if matched {
                input = input
                    .get(last_cap..)
                    .expect("Error geting input slice")
                    .to_string();
                pos += last_cap;
                break;
            }
        }
        if !matched {
            panic!(
                "Unexpected character: \"{}\" at position {}",
                input
                    .chars()
                    .nth(0)
                    .expect("Error geting character at position 0"),
                pos
            );
        }
    }
    if !input.is_empty() {
        return Err(format!("Could not tokenize: {}", input));
    }
    Ok(tokens)
}

fn check_parens(tokens: &VecDeque<Token>) -> Result<(), String> {
    #[derive(Debug, PartialEq)]
    enum Parens {
        Paren,
        Bracket,
    };
    let mut pstack = Vec::new();
    for c in tokens {
        match c {
            LParen => pstack.push(Parens::Paren),
            RParen => {
                if pstack.is_empty() || pstack.pop().unwrap() != Parens::Paren {
                    return Err(format!("Invalid parenthesis in expression: {:?}", tokens));
                }
            },
            LBrack => pstack.push(Parens::Bracket),
            RBrack => {
                if pstack.is_empty() || pstack.pop().unwrap() != Parens::Bracket {
                    return Err(format!("Invalid bracket in expression: {:?}", tokens));
                }
            },
            _ => (),
        }
    }
    if !pstack.is_empty() {
        match pstack.pop().unwrap() {
            Parens::Paren => {
                return Err(format!("Unmatched parenthesis in expression: {:?}", tokens))
            },
            Parens::Bracket => return Err(format!("Unmatched bracket in expression: {:?}", tokens)),
        }
    }
    Ok(())
}

pub fn to_expr(tokens: &mut VecDeque<Token>) -> Result<VecDeque<Token>, String> {
    check_parens(tokens)?;
    let toks = tokens.clone();
    let tokens = to_expr_inner(tokens)?;
    info!("to_expr_inner({:?}) => {:?}", toks, tokens);
    Ok(tokens)
}

fn to_expr_inner(tokens: &mut VecDeque<Token>) -> Result<VecDeque<Token>, String> {
    let mut ret = VecDeque::new();
    let mut is_sym = false;
    let mut is_mem = false;
    let mut had_blank = false;
    while let Some(t) = tokens.pop_front() {
        match t {
            LParen => {
                let expr = to_expr_inner(tokens)?;
                if (is_sym | is_mem) & had_blank {
                    return Err(format!("Invalid Symtax near: {:?}", tokens));
                }
                if is_mem & !had_blank {
                    ret.push_back(MemExpr(expr));
                } else if is_sym & !had_blank {
                    ret.push_back(SymExpr(expr));
                } else {
                    ret.push_back(Expr(expr));
                }
                is_sym = false;
                is_mem = false;
                had_blank = false;
            },
            LBrack => {
                if had_blank || ret.is_empty() {
                    ret.push_back(List(to_expr_inner(tokens)?));
                } else {
                    let mut idx = to_expr_inner(tokens)?;
                    let idx = if idx.len() > 1 {
                        Token::Expr(idx)
                    } else {
                        idx.pop_front().unwrap_or(Token::Null)
                    };
                    let idx_target = ret.pop_back().unwrap();
                    ret.push_back(IndexExpr(Box::new(idx_target), Box::new(idx)));
                }
                is_sym = false;
                is_mem = false;
                had_blank = false;
            },
            RBrack | RParen => {
                break;
            },
            Quote => {
                is_sym = true;
                had_blank = false;
            },
            Ptr => {
                is_mem = true;
                had_blank = false;
            },
            Comma => (),
            Ident(name) => {
                if is_sym {
                    ret.push_back(Sym(name));
                } else {
                    ret.push_back(Ident(name));
                }
                is_sym = false;
                is_mem = false;
                had_blank = false;
            },
            Blank => {
                had_blank = true;
            },
            other => {
                ret.push_back(other);
                is_sym = false;
                is_mem = false;
                had_blank = false;
            },
        };
    }
    Ok(ret)
}

#[cfg(test)]
mod tests {
    use self::test::Bencher;
    use super::*;
    #[test]
    fn lexer_1() {
        let goal = {
            use Token::*;
            VecDeque::from(vec![
                LParen,
                Op("+".to_string()),
                Blank,
                Num(1.0),
                Blank,
                Num(2.0),
                RParen,
            ])
        };
        let tokens = tokenize("(+ 1 2)");
        assert_eq!(tokens, Ok(goal));
    }

    #[test]
    fn lexer_2() {
        let goal = {
            use Token::*;
            VecDeque::from(vec![
                LParen,
                Op("+".to_string()),
                Blank,
                LParen,
                Op("*".to_string()),
                Blank,
                Num(8.0),
                Blank,
                Num(0.5),
                RParen,
                Blank,
                LParen,
                Op("*".to_string()),
                Blank,
                Num(2.0),
                Blank,
                Num(4.0),
                RParen,
                RParen,
            ])
        };
        let tokens = tokenize("(+ (* 8 0.5) (* 2 4))");
        assert_eq!(tokens, Ok(goal));
    }

    #[test]
    fn nest_1() {
        let goal = {
            use Token::*;
            VecDeque::from(vec![Expr(VecDeque::from(vec![
                Op("+".to_string()),
                Num(1.0),
                Num(2.0),
            ]))])
        };

        let tokens = to_expr(&mut tokenize("(+ 1 2)").unwrap()).unwrap();
        assert_eq!(tokens, goal);
    }

    #[test]
    fn nest_2() {
        let goal = {
            use Token::*;
            VecDeque::from(vec![Expr(VecDeque::from(vec![
                Op("+".to_string()),
                Expr(VecDeque::from(vec![
                    Op("*".to_string()),
                    Num(8.0),
                    Num(0.5),
                ])),
                Expr(VecDeque::from(vec![
                    Op("*".to_string()),
                    Num(2.0),
                    Num(4.0),
                ])),
            ]))])
        };

        let tokens = to_expr(&mut tokenize("(+ (* 8 0.5) (* 2 4))").unwrap()).unwrap();
        assert_eq!(tokens, goal);
    }

    #[bench]
    fn lex_bench_lex(b: &mut Bencher) {
        let s = "(+ (* 8 0.5) (* 2 4))";
        b.iter(|| tokenize(s));
    }

    #[bench]
    fn lex_bench_to_expr(b: &mut Bencher) {
        let s = "(+ (* 8 0.5) (* 2 4))";
        let l = tokenize(s).unwrap();
        b.iter(|| to_expr(&mut l.clone()));
    }

    #[bench]
    fn lex_bench_lex_to_expr(b: &mut Bencher) {
        let s = "(+ (* 8 0.5) (* 2 4))";
        b.iter(|| to_expr(&mut tokenize(s).unwrap()));
    }
}
