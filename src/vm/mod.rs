mod hw;
use std::{
    collections::BTreeMap,
    io::Cursor,
};

use byteorder::{
    LittleEndian,
    ReadBytesExt,
    WriteBytesExt,
};

use std::cmp::Ordering;

use vm::hw::{
    IODevice,
    MMIO,
    RNG,
    TTY,
    VGA,
};

enum ByteVal {
    U8(u8),
    U16(u16),
    U32(u32),
    U64(u64),
    I8(i8),
    I16(i16),
    I32(i32),
    I64(i64),
}

impl ByteVal {
    fn write(&self, buf: &mut Vec<u8>) {
        use self::ByteVal::*;
        match self {
            U8(n) => buf.write_u8(*n).unwrap(),
            U16(n) => buf.write_u16::<LittleEndian>(*n).unwrap(),
            U32(n) => buf.write_u32::<LittleEndian>(*n).unwrap(),
            U64(n) => buf.write_u64::<LittleEndian>(*n).unwrap(),
            I8(n) => buf.write_i8(*n).unwrap(),
            I16(n) => buf.write_i16::<LittleEndian>(*n).unwrap(),
            I32(n) => buf.write_i32::<LittleEndian>(*n).unwrap(),
            I64(n) => buf.write_i64::<LittleEndian>(*n).unwrap(),
        }
    }
}

#[derive(Debug, PartialEq)]
enum VmCmp {
    Lt,
    Eq,
    Gt,
    None,
}
pub struct VM {
    ip:   usize,
    sp:   usize,
    regs: [u64; 16],
    flag: VmCmp,
    mem:  [u8; 1024 * 8],
    io:   Vec<IODevice>,
}

#[derive(Debug, Clone)]
pub enum Src {
    Reg(u8),
    Mem(usize),
    Deref(u8),
    Offset(u8, usize),
    Imm(u64),
    Label(String),
    LabelOffset(String, usize),
}

#[derive(Debug, Clone)]
pub enum Dst {
    Reg(u8),
    Deref(u8),
    Offset(u8, usize),
    Mem(usize),
    Label(String),
    LabelOffset(String, usize),
}

impl Src {
    fn tag(&self) -> u8 {
        use self::Src::*;
        return match self {
            Imm(..) => 0x00,
            Reg(..) => 0x01,
            Mem(..) => 0x02,
            Deref(..) => 0x03,
            Offset(..) => 0x04,
            other => 0xff,
        };
    }

    fn encode(&self) -> Vec<ByteVal> {
        use self::{
            ByteVal::*,
            Src::*,
        };
        let mut ret: Vec<ByteVal> = Vec::new();
        ret.push(U8(self.tag()));
        match self {
            Reg(n) | Deref(n) => ret.push(U8(*n)),
            Mem(n) => ret.push(U64(*n as u64)),
            Imm(n) => ret.push(U64(*n)),
            Offset(n, d) => {
                ret.push(U8(*n));
                ret.push(U64(*d as u64));
            },
            _ => ret.push(U64(0xffffffff)),
        };
        ret
    }

    fn decode(pgm: &mut Cursor<&[u8]>) -> Option<Src> {
        use self::Src::*;
        let opcode = match pgm.read_u8() {
            Ok(n) => n,
            Err(_) => return None,
        };
        let ret = match opcode {
            0x00 => Imm(pgm.read_u64::<LittleEndian>().unwrap()),
            0x01 => Reg(pgm.read_u8().unwrap()),
            0x02 => Mem(pgm.read_u64::<LittleEndian>().unwrap() as usize),
            0x03 => Deref(pgm.read_u8().unwrap()),
            0x04 => Offset(
                pgm.read_u8().unwrap(),
                pgm.read_u64::<LittleEndian>().unwrap() as usize,
            ),
            other => panic!("Invalid Opcode: {}", other),
        };
        Some(ret)
    }
}

impl Dst {
    fn tag(&self) -> u8 {
        use self::Dst::*;
        return match self {
            Reg(..) => 0x00,
            Mem(..) => 0x01,
            Deref(..) => 0x02,
            Offset(..) => 0x03,
            other => 0xff,
        };
    }

    fn encode(&self) -> Vec<ByteVal> {
        use self::{
            ByteVal::*,
            Dst::*,
        };
        let mut ret: Vec<ByteVal> = Vec::new();
        ret.push(U8(self.tag()));
        match self {
            Reg(n) | Deref(n) => ret.push(U8(*n)),
            Mem(n) => ret.push(U64(*n as u64)),
            Offset(n, d) => {
                ret.push(U8(*n));
                ret.push(U64(*d as u64));
            },
            other => ret.push(U64(0xffffffffffffffff)),
        };
        ret
    }

    fn decode(pgm: &mut Cursor<&[u8]>) -> Option<Dst> {
        use self::Dst::*;
        let opcode = match pgm.read_u8() {
            Ok(n) => n,
            Err(_) => return None,
        };
        let ret = match opcode {
            0x00 => Reg(pgm.read_u8().unwrap()),
            0x01 => Mem(pgm.read_u64::<LittleEndian>().unwrap() as usize),
            0x02 => Deref(pgm.read_u8().unwrap()),
            0x03 => Offset(
                pgm.read_u8().unwrap(),
                pgm.read_u64::<LittleEndian>().unwrap() as usize,
            ),
            other => panic!("Invalid Opcode: {}", other),
        };
        Some(ret)
    }
}

#[derive(Debug, Clone)]
pub enum Inst {
    Nop,
    Add(Dst, Src),
    Sub(Dst, Src),
    Mul(Dst, Src),
    Div(Dst, Src),
    Pow(Dst, Src),
    Mod(Dst, Src),

    FAdd(Dst, Src),
    FSub(Dst, Src),
    FMul(Dst, Src),
    FDiv(Dst, Src),
    FPow(Dst, Src),
    FMod(Dst, Src),

    And(Dst, Src),
    Or(Dst, Src),
    Xor(Dst, Src),
    Not(Dst),
    Shr(Dst, Src),
    Shl(Dst, Src),

    Ret,

    Cmp(Src, Src),
    FCmp(Src, Src),
    Clf,

    Jmp(Src),
    JGt(Src),
    JGe(Src),
    JEq(Src),
    JNe(Src),
    JLe(Src),
    JLt(Src),

    Call(Src),
    Push(Src),
    Pop(Dst),
    Peek(Dst),
    Xchg(Dst, Dst),
    Mov(Dst, Src),
    Drop,
    Hlt,
    Label(String),
}

impl Inst {
    fn opcode(&self) -> u16 {
        use self::Inst::*;
        match self {
            Nop => 0x000,
            Add(..) => 0x100,
            Sub(..) => 0x101,
            Mul(..) => 0x102,
            Div(..) => 0x103,
            Pow(..) => 0x104,
            Mod(..) => 0x105,

            FAdd(..) => 0x110,
            FSub(..) => 0x111,
            FMul(..) => 0x112,
            FDiv(..) => 0x113,
            FPow(..) => 0x114,
            FMod(..) => 0x115,

            And(..) => 0x120,
            Or(..) => 0x121,
            Xor(..) => 0x122,
            Not(..) => 0x123,
            Shr(..) => 0x124,
            Shl(..) => 0x125,

            Ret => 0x200,
            Call(..) => 0x201,

            Cmp(..) => 0x300,
            FCmp(..) => 0x310,
            Clf => 0x320,

            Jmp(..) => 0x400,
            JGt(..) => 0x410,
            JGe(..) => 0x411,
            JEq(..) => 0x401,
            JNe(..) => 0x402,
            JLe(..) => 0x421,
            JLt(..) => 0x420,

            Push(..) => 0x500,
            Pop(..) => 0x501,
            Peek(..) => 0x502,
            Drop => 0x510,

            Mov(..) => 0x600,
            Xchg(..) => 0x601,

            Hlt => 0xfff,
            other => panic!("Found {:?}, this should not happend", other),
        }
    }

    fn encode(&self) -> Vec<ByteVal> {
        use self::{
            ByteVal::*,
            Inst::*,
        };
        let mut ret: Vec<ByteVal> = Vec::new();
        if let Inst::Label(_) = self {
            return ret;
        }
        ret.push(U16(self.opcode()));
        match self {
            Add(dst, src)
            | Sub(dst, src)
            | Mul(dst, src)
            | Div(dst, src)
            | Pow(dst, src)
            | Mod(dst, src)
            | FAdd(dst, src)
            | FSub(dst, src)
            | FMul(dst, src)
            | FDiv(dst, src)
            | FPow(dst, src)
            | FMod(dst, src)
            | And(dst, src)
            | Or(dst, src)
            | Xor(dst, src)
            | Shr(dst, src)
            | Shl(dst, src)
            | Mov(dst, src) => {
                ret.append(&mut dst.encode());
                ret.append(&mut src.encode());
            },
            Xchg(dst1, dst2) => {
                ret.append(&mut dst1.encode());
                ret.append(&mut dst2.encode());
            },
            Cmp(src1, src2) | FCmp(src1, src2) => {
                ret.append(&mut src1.encode());
                ret.append(&mut src2.encode());
            },

            Jmp(src) | JEq(src) | JNe(src) | JGe(src) | JGt(src) | JLe(src) | JLt(src)
            | Call(src) | Push(src) => ret.append(&mut src.encode()),
            Pop(dst) | Peek(dst) | Not(dst) => ret.append(&mut dst.encode()),

            Nop | Ret | Clf | Drop | Hlt | Label(..) => (),
        };
        ret
    }

    fn decode(pgm: &mut Cursor<&[u8]>) -> Option<Inst> {
        use self::Inst::*;
        let opcode = match pgm.read_u16::<LittleEndian>() {
            Ok(val) => val,
            Err(_) => return None,
        };
        let ret = match opcode {
            0x000 => Nop,
            0x100 => Add(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x101 => Sub(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x102 => Mul(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x103 => Div(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x104 => Pow(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x105 => Mod(Dst::decode(pgm)?, Src::decode(pgm)?),

            0x110 => FAdd(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x111 => FSub(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x112 => FMul(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x113 => FDiv(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x114 => FPow(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x115 => FMod(Dst::decode(pgm)?, Src::decode(pgm)?),

            0x120 => And(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x121 => Or(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x122 => Xor(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x123 => Not(Dst::decode(pgm)?),
            0x124 => Shr(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x125 => Shl(Dst::decode(pgm)?, Src::decode(pgm)?),

            0x200 => Ret,
            0x201 => Call(Src::decode(pgm)?),

            0x300 => Cmp(Src::decode(pgm)?, Src::decode(pgm)?),
            0x310 => FCmp(Src::decode(pgm)?, Src::decode(pgm)?),
            0x320 => Clf,

            0x400 => Jmp(Src::decode(pgm)?),
            0x410 => JGt(Src::decode(pgm)?),
            0x411 => JGe(Src::decode(pgm)?),
            0x401 => JEq(Src::decode(pgm)?),
            0x402 => JNe(Src::decode(pgm)?),
            0x421 => JLe(Src::decode(pgm)?),
            0x420 => JLt(Src::decode(pgm)?),

            0x500 => Push(Src::decode(pgm)?),
            0x501 => Pop(Dst::decode(pgm)?),
            0x502 => Peek(Dst::decode(pgm)?),
            0x510 => Drop,

            0x600 => Mov(Dst::decode(pgm)?, Src::decode(pgm)?),
            0x601 => Xchg(Dst::decode(pgm)?, Dst::decode(pgm)?),
            0xfff => Hlt,
            _ => return None,
        };
        Some(ret)
    }
}

impl VM {
    fn new() -> VM {
        VM {
            ip:   0,
            flag: VmCmp::None,
            sp:   (1024 * 8),
            regs: [0; 16],
            mem:  [0; 1024 * 8],
            io:   Vec::new(),
        }
    }

    fn read_src(&mut self, src: &Src) -> u64 {
        use self::Src::*;
        match src {
            Mem(addr) => self.read_val(*addr),
            Reg(rn) => self.regs[*rn as usize],
            Deref(rn) => {
                let addr = self.regs[*rn as usize];
                self.read_val(addr as usize)
            },
            Offset(rn, off) => {
                let addr = self.regs[*rn as usize] + *off as u64;
                self.read_val(addr as usize)
            },
            Imm(n) => *n,
            other => panic!("Found {:?}, this should not happend", other),
        }
    }

    fn read_dst(&mut self, dst: &Dst) -> u64 {
        use self::Dst::*;
        match dst {
            Mem(addr) => self.read_val(*addr),
            Reg(rn) => self.regs[*rn as usize],
            Deref(rn) => {
                let addr = self.regs[*rn as usize];
                self.read_val(addr as usize)
            },
            Offset(rn, off) => {
                let addr = self.regs[*rn as usize] + *off as u64;
                self.read_val(addr as usize)
            },
            other => panic!("Found {:?}, this should not happend", other),
        }
    }

    fn write_dst(&mut self, dst: &Dst, val: u64) {
        use self::Dst::*;
        match dst {
            Reg(rn) => {
                self.regs[*rn as usize] = val;
            },
            Deref(rn) => {
                let addr = self.regs[*rn as usize];
                self.write_val(val, addr as usize);
            },
            Offset(rn, off) => {
                let addr = self.regs[*rn as usize] + *off as u64;
                self.write_val(val, addr as usize);
            },
            Mem(addr) => {
                self.write_val(val, *addr);
            },
            other => panic!("Found {:?}, this should not happend", other),
        }
    }

    fn load(&mut self, pgm: &[u8], base: u64) {
        let base = base as usize;
        for (i, v) in pgm.iter().enumerate() {
            self.mem[base + i] = *v;
        }
    }

    fn decode(&mut self) -> Option<Inst> {
        if self.ip > self.mem.len() {
            return None;
        }
        let mut c = Cursor::new(&self.mem[..]);
        c.set_position(self.ip as u64);
        let inst = Inst::decode(&mut c);
        self.ip = c.position() as usize;
        inst
    }

    fn step(&mut self) -> bool {
        match self.decode() {
            Some(inst) => self.exec(&inst),
            None => false,
        }
    }

    fn attach(&mut self, dev: IODevice) -> Result<(), String> {
        let start = dev.addr;
        let end = dev.addr + dev.size();
        if start <= self.mem.len() {
            return Err(format!(
                "Can't attach device [{}], overlaps with main memory",
                dev
            ));
        }
        for o_dev in &self.io {
            let o_start = o_dev.addr;
            let o_end = o_dev.addr + o_dev.size();
            if (start <= o_end) && (o_start <= end) {
                return Err(format!(
                    "Can't attach device [{}], overlaps with [{}]",
                    dev, o_dev
                ));
            }
        }
        self.io.push(dev);
        Ok(())
    }

    fn write_val(&mut self, val: u64, addr: usize) {
        for mut dev in &mut self.io {
            let size = dev.size();
            if addr >= dev.addr && addr < dev.addr + size {
                dev.write(addr, val);
                return;
            }
        }
        let mut val = val;
        for i in 0..8 {
            self.mem[addr + i] = (val & 0xff) as u8;
            val >>= 8;
        }
    }

    fn read_val(&mut self, addr: usize) -> u64 {
        for mut dev in &mut self.io {
            let size = dev.size();
            if addr >= dev.addr && addr < dev.addr + size {
                return dev.read(addr);
            }
        }
        let mut val: u64 = 0;
        for i in 0..8 {
            val <<= 8;
            val += u64::from(self.mem[addr + (7 - i)]);
        }
        val
    }

    fn s_push(&mut self, val: u64) {
        self.sp -= 8;
        let addr = self.sp;
        self.write_val(val, addr);
    }

    fn s_pop(&mut self) -> u64 {
        let addr = self.sp;
        self.sp += 8;
        self.read_val(addr)
    }

    fn s_peek(&mut self) -> u64 {
        let addr = self.sp;
        self.read_val(addr)
    }

    fn pop(&mut self, dst: &Dst) {
        let val = self.s_pop();
        self.write_dst(dst, val);
    }

    fn peek(&mut self, dst: &Dst) {
        let val = self.s_peek();
        self.write_dst(dst, val);
    }

    fn push(&mut self, src: &Src) {
        let val = self.read_src(src);
        self.s_push(val);
    }

    fn op(&mut self, dst: &Dst, src: &Src, op: &Fn(u64, u64) -> u64) {
        let dst_v = self.read_dst(dst);
        let src_v = self.read_src(src);
        self.write_dst(dst, op(dst_v, src_v));
    }

    fn fop(&mut self, dst: &Dst, src: &Src, op: &Fn(f64, f64) -> f64) {
        let dst_f = f64::from_bits(self.read_dst(dst));
        let src_f = f64::from_bits(self.read_src(src));
        let res = op(dst_f, src_f);
        self.write_dst(dst, res.to_bits());
    }

    fn xchg(&mut self, v1: &Dst, v2: &Dst) {
        let v1_ = self.read_dst(v1);
        let v2_ = self.read_dst(v2);
        self.write_dst(v2, v1_);
        self.write_dst(v1, v2_);
    }

    fn mov(&mut self, dst: &Dst, src: &Src) {
        let val = self.read_src(src);
        self.write_dst(dst, val);
    }

    fn jmp(&mut self, addr: &Src) {
        let addr = self.read_src(addr);
        self.ip = addr as usize;
    }

    fn exec(&mut self, inst: &Inst) -> bool {
        use self::Inst::*;
        // println!("EXEC: {:?}",inst);
        // self.print();
        // println!("==================");
        match inst {
            Nop => (),
            Hlt => return false,
            Drop => self.sp += 8,
            Mov(dst, src) => self.mov(dst, src),
            Xchg(v1, v2) => self.xchg(v1, v2),
            Add(dst, src) => self.op(dst, src, &|a, b| a + b),
            Sub(dst, src) => self.op(dst, src, &|a, b| a - b),
            Mul(dst, src) => self.op(dst, src, &|a, b| a * b),
            Div(dst, src) => self.op(dst, src, &|a, b| a / b),
            Pow(dst, src) => self.op(dst, src, &|a, b| a.pow(b as u32)),
            Mod(dst, src) => self.op(dst, src, &|a, b| a % b),

            FAdd(dst, src) => self.fop(dst, src, &|a, b| a + b),
            FSub(dst, src) => self.fop(dst, src, &|a, b| a - b),
            FMul(dst, src) => self.fop(dst, src, &|a, b| a * b),
            FDiv(dst, src) => self.fop(dst, src, &|a, b| a / b),
            FPow(dst, src) => self.fop(dst, src, &|a, b| a.powf(b)),
            FMod(dst, src) => self.fop(dst, src, &|a, b| a % b),

            And(dst, src) => self.op(dst, src, &|a, b| a & b),
            Or(dst, src) => self.op(dst, src, &|a, b| a | b),
            Xor(dst, src) => self.op(dst, src, &|a, b| a ^ b),
            Shr(dst, src) => self.op(dst, src, &|a, b| a >> b),
            Shl(dst, src) => self.op(dst, src, &|a, b| a << b),
            Not(dst) => self.op(dst, &Src::Imm(0), &|a, _| !a),
            Push(src) => self.push(src),
            Pop(dst) => self.pop(dst),
            Peek(dst) => self.peek(dst),

            Call(src) => {
                let addr = self.ip;
                self.s_push(addr as u64);
                let addr = self.read_src(src) as usize;
                self.ip = addr;
            },

            Ret => {
                let addr = self.s_pop();
                self.ip = addr as usize;
            },

            Clf => self.flag = VmCmp::None,

            FCmp(a, b) => {
                use std::cmp::Ordering;
                let a = f64::from_bits(self.read_src(a));
                let b = f64::from_bits(self.read_src(b));
                match fcmp(&a, &b) {
                    Ordering::Greater => self.flag = VmCmp::Gt,
                    Ordering::Equal => self.flag = VmCmp::Eq,
                    Ordering::Less => self.flag = VmCmp::Lt,
                };
            },

            Cmp(a, b) => {
                let a = self.read_src(a);
                let b = self.read_src(b);
                match a.cmp(&b) {
                    Ordering::Greater => self.flag = VmCmp::Gt,
                    Ordering::Equal => self.flag = VmCmp::Eq,
                    Ordering::Less => self.flag = VmCmp::Lt,
                }
            },

            Jmp(src) => self.jmp(src),

            JGt(addr) => {
                if let VmCmp::Gt = self.flag {
                    self.jmp(addr);
                }
            },

            JGe(addr) => match self.flag {
                VmCmp::Gt | VmCmp::Eq => {
                    self.jmp(addr);
                },
                _ => (),
            },

            JEq(addr) => match self.flag {
                VmCmp::Eq => {
                    self.jmp(addr);
                },
                _ => (),
            },

            JNe(addr) => match self.flag {
                VmCmp::Gt | VmCmp::Lt => {
                    self.jmp(addr);
                },
                _ => (),
            },

            JLe(addr) => match self.flag {
                VmCmp::Lt | VmCmp::Eq => {
                    self.jmp(addr);
                },
                _ => (),
            },

            JLt(addr) => {
                if let VmCmp::Lt = self.flag {
                    self.jmp(addr);
                }
            },
            other => panic!("Found {:?}, this should not happend", other),
        }
        return true;
    }

    fn run(&mut self) {
        use std::time::Instant;
        let mut cnt: u64 = 0;
        let now = Instant::now();
        while self.step() {
            cnt += 1;
            if cnt % 10000000 == 0 {
                let n = (cnt as f64) / 1000000.0;
                let t = now.elapsed();
                let t = ((t.as_secs() * 1000 + u64::from(t.subsec_millis())) as f64) / 1000.0;
                info!("{} Mips", n / t);
            }
        }
    }

    fn print(&self) {
        println!("{} Device(s):", self.io.len());
        for dev in &self.io {
            println!("- {}", dev);
        }
        let fregs: Vec<f64> = self.regs.iter().map(|v| f64::from_bits(*v)).collect();
        println!("IP:   0x{:x}", self.ip);
        println!("SP:   0x{:x}", self.sp);
        println!("Regs:  {:x?}", self.regs);
        println!("FRegs: {:?}", fregs);
        println!("Flag:  [{:?}]", self.flag);
        println!("Stack: {:?}", &self.mem[self.sp..]);
    }

    fn resolve_label(&self, labels: &BTreeMap<String, u64>, inst: &mut Inst) {
        use self::Inst::*;
        match inst {
            Add(ref mut dst, ref mut src)
            | Sub(ref mut dst, ref mut src)
            | Mul(ref mut dst, ref mut src)
            | Div(ref mut dst, ref mut src)
            | Pow(ref mut dst, ref mut src)
            | Mod(ref mut dst, ref mut src)
            | FAdd(ref mut dst, ref mut src)
            | FSub(ref mut dst, ref mut src)
            | FMul(ref mut dst, ref mut src)
            | FDiv(ref mut dst, ref mut src)
            | FPow(ref mut dst, ref mut src)
            | FMod(ref mut dst, ref mut src)
            | And(ref mut dst, ref mut src)
            | Or(ref mut dst, ref mut src)
            | Xor(ref mut dst, ref mut src)
            | Shr(ref mut dst, ref mut src)
            | Shl(ref mut dst, ref mut src)
            | Mov(ref mut dst, ref mut src) => {
                match src.clone() {
                    Src::Label(ref name) => {
                        *src = Src::Imm(*labels.get(name).unwrap());
                    },
                    Src::LabelOffset(ref name, offset) => {
                        *src = Src::Imm(*labels.get(name).unwrap() + offset as u64);
                    },
                    _ => (),
                }
                match dst.clone() {
                    Dst::Label(ref name) => {
                        *dst = Dst::Mem(*labels.get(name).unwrap() as usize);
                    },
                    Dst::LabelOffset(ref name, offset) => {
                        *dst = Dst::Mem(*labels.get(name).unwrap() as usize + offset);
                    },
                    _ => (),
                }
            },
            Xchg(ref mut dst1, ref mut dst2) => {
                match dst1.clone() {
                    Dst::Label(ref name) => {
                        *dst1 = Dst::Mem(*labels.get(name).unwrap() as usize);
                    },
                    Dst::LabelOffset(ref name, offset) => {
                        *dst1 = Dst::Mem(*labels.get(name).unwrap() as usize + offset);
                    },
                    _ => (),
                }
                match dst2.clone() {
                    Dst::Label(ref name) => {
                        *dst2 = Dst::Mem(*labels.get(name).unwrap() as usize);
                    },
                    Dst::LabelOffset(ref name, offset) => {
                        *dst2 = Dst::Mem(*labels.get(name).unwrap() as usize + offset);
                    },
                    _ => (),
                }
            },
            Cmp(ref mut src1, ref mut src2) | FCmp(ref mut src1, ref mut src2) => {
                match src1.clone() {
                    Src::Label(ref name) => {
                        *src1 = Src::Imm(*labels.get(name).unwrap());
                    },
                    Src::LabelOffset(ref name, offset) => {
                        *src1 = Src::Imm(*labels.get(name).unwrap() + offset as u64);
                    },
                    _ => (),
                }

                match src2.clone() {
                    Src::Label(ref name) => {
                        *src2 = Src::Imm(*labels.get(name).unwrap());
                    },
                    Src::LabelOffset(ref name, offset) => {
                        *src2 = Src::Imm(*labels.get(name).unwrap() + offset as u64);
                    },
                    _ => (),
                }
            },

            Jmp(ref mut src) | JEq(ref mut src) | JNe(ref mut src) | JGe(ref mut src)
            | JGt(ref mut src) | JLe(ref mut src) | JLt(ref mut src) | Call(ref mut src)
            | Push(ref mut src) => match src.clone() {
                Src::Label(ref name) => {
                    *src = Src::Imm(*labels.get(name).unwrap());
                },
                Src::LabelOffset(ref name, offset) => {
                    *src = Src::Imm(*labels.get(name).unwrap() + offset as u64);
                },
                _ => (),
            },
            Pop(ref mut dst) | Peek(ref mut dst) | Not(ref mut dst) => match dst.clone() {
                Dst::Label(ref name) => {
                    *dst = Dst::Mem(*labels.get(name).unwrap() as usize);
                },
                Dst::LabelOffset(ref name, offset) => {
                    *dst = Dst::Mem(*labels.get(name).unwrap() as usize + offset);
                },
                _ => (),
            },

            Nop | Ret | Clf | Drop | Hlt | Label(..) => (),
        };
    }

    fn assemble(&mut self, pgm: &mut Vec<Inst>) -> Vec<u8> {
        use std::collections::BTreeMap;
        let mut code: Vec<u8> = Vec::new();
        let mut labels: BTreeMap<String, u64> = BTreeMap::new();
        for inst in pgm.iter() {
            if let Inst::Label(name) = inst {
                labels.insert(name.clone(), code.len() as u64);
                continue;
            }
            for v in inst.encode() {
                v.write(&mut code);
            }
        }
        println!("{:?}", labels);
        code.clear();
        for mut inst in pgm.iter_mut() {
            if let Inst::Label(name) = inst {
                continue;
            }
            self.resolve_label(&labels, &mut inst);
            debug!("{:?} @ 0x{:x}", inst, code.len());
            for v in inst.encode() {
                v.write(&mut code);
            }
        }
        code
    }
}

fn fcmp(a: f64, b: f64) -> Ordering {
    match (a, b) {
        (x, y) if x.is_nan() && y.is_nan() => Ordering::Equal,
        (x, _) if x.is_nan() => Ordering::Greater,
        (_, y) if y.is_nan() => Ordering::Less,
        (..) => a.partial_cmp(b).unwrap(),
    }
}

pub fn test() {
    let mut vm = VM::new();
    let insts = vec![
        Inst::Label("BEGIN".to_string()),
        Inst::Mov(Dst::Reg(1), Src::Mem(0x3000)),
        Inst::And(Dst::Reg(1), Src::Imm(0xffffffff)),
        Inst::Mov(Dst::Offset(0, 0x10000), Src::Reg(1)),
        Inst::Add(Dst::Reg(0), Src::Imm(1)),
        Inst::Mov(Dst::Reg(2), Src::Reg(0)),
        Inst::Mod(Dst::Reg(2), Src::Imm(256 * 256)),
        Inst::Cmp(Src::Reg(2), Src::Imm(256 * 256 - 1)),
        Inst::Mod(Dst::Reg(0), Src::Imm(256 * 256)),
        Inst::JNe(Src::Label("BEGIN".to_string())),
        Inst::Mov(
            Dst::Mem(0x10000),
            Src::Imm(0b00001000000000000000000000000000 << 32),
        ),
        Inst::Jmp(Src::Label("BEGIN".to_string())),
    ];
    let mut insts = insts.clone();
    println!("VM Instructions:",);
    for ins in &insts {
        println!("   {:?}", ins);
    }
    let pgm = vm.assemble(&mut insts);
    println!("Bytecode: {:?}", &pgm);
    vm.attach(IODevice {
        name:   "RNG".to_string(),
        addr:   0x3000,
        device: Box::new(RNG::new()),
    })
    .unwrap();
    vm.attach(IODevice {
        name:   "TTY".to_string(),
        addr:   0x4000,
        device: Box::new(TTY::new()),
    })
    .unwrap();
    vm.attach(IODevice {
        name:   "Screen".to_string(),
        addr:   0x10000,
        device: Box::new(VGA::new(256, 256)),
    })
    .unwrap();
    vm.load(&pgm, 0x0);
    vm.print();
    println!("==========");
    vm.run();
    println!("==========");
    vm.print();
}
