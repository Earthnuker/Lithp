use minifb::{
    Key,
    Scale,
    Window,
    WindowOptions,
};
use std::fmt::{
    self,
    Debug,
};
use vm::hw::MMIO;

bitflags! {
    struct CW: u32 {
        const SetScale   = 0b10000000000000000000000000000000;
        const SetWidth   = 0b01000000000000000000000000000000;
        const SetHeight  = 0b00100000000000000000000000000000;
        const AutoRender = 0b00010000000000000000000000000000;
        const Render     = 0b00001000000000000000000000000000;
    }
}

#[derive(Derivative)]
#[derivative(Debug)]
pub struct VGA {
    #[derivative(Debug = "ignore")]
    buffer: Vec<u32>,
    keys: Vec<Key>,
    width: usize,
    height: usize,
    scale: Scale,
    cw: CW,
    win: Option<Window>,
}

impl VGA {
    pub fn new(width: usize, height: usize) -> VGA {
        VGA {
            width,
            height,
            buffer: vec![0u32; width * height],
            win: None,
            cw: CW::empty(),
            keys: Vec::new(),
            scale: Scale::X2,
        }
    }

    fn refresh(&mut self) {
        self.check();
        if let Some(ref mut w) = self.win {
            w.update_with_buffer(&self.buffer).unwrap();
        }
    }

    fn check(&mut self) {
        if match &self.win {
            Some(win) if win.is_open() => false,
            _ => true,
        } {
            self.respawn();
        }
    }

    fn respawn(&mut self) {
        let wo = WindowOptions {
            borderless: true,
            scale:      self.scale,
            resize:     false,
            title:      true,
        };
        let mut win = Window::new("VScreen", self.width, self.height, wo).unwrap();
        self.buffer.resize(self.width * self.height, 0u32);
        win.update_with_buffer(&self.buffer).unwrap();
        self.win = Some(win);
    }

    fn update_cw(&mut self, val: u64) {
        let flags = CW::from_bits_truncate(((val & 0xffffffff00000000) >> 32) as u32);
        let val = (val & 0xffffffff) as u32;
        trace!("Update CW: 0x{:x}=({:?}) ({})", flags.bits(), flags, val);
        if flags.contains(CW::Render) {
            self.refresh();
            return;
        }
        if flags.contains(CW::SetScale) {
            self.scale = match val {
                0 => Scale::X1,
                1 => Scale::X2,
                2 => Scale::X4,
                3 => Scale::X16,
                4 => Scale::X32,
                _ => {
                    return;
                },
            };
            self.respawn();
            return;
        };
        if flags.contains(CW::SetWidth) {
            self.width = val as usize;
            self.respawn();
            return;
        }
        if flags.contains(CW::SetHeight) {
            self.height = val as usize;
            self.respawn();
            return;
        }
        self.cw ^= flags;
        // self.respawn();
    }
}

impl MMIO for VGA {
    fn write(&mut self, addr: usize, val: u64) {
        match self.win {
            None => self.respawn(),
            Some(_) => (),
        }
        if val & 0xffffffff00000000 != 0 {
            self.update_cw(val);
        } else {
            self.buffer[addr] = (val & 0xffffffff) as u32;
            if self.cw.intersects(CW::AutoRender) {
                self.refresh();
            }
        }
    }

    fn read(&mut self, addr: usize) -> u64 {
        let val = self.buffer[addr] as u64;
        val | (u64::from(self.cw.bits()) << 32)
    }

    fn size(&self) -> usize {
        self.width * self.height
    }
}
