mod rng;
mod tty;
mod vga;
pub use self::{
    rng::RNG,
    tty::TTY,
    vga::VGA,
};
use std::fmt::{
    self,
    Display,
};
pub trait MMIO {
    fn write(&mut self, usize, u64);
    fn read(&mut self, usize) -> u64;
    fn size(&self) -> usize;
}

pub struct IODevice {
    pub name:   String,
    pub addr:   usize,
    pub device: Box<MMIO>,
}

impl MMIO for IODevice {
    fn write(&mut self, addr: usize, val: u64) {
        self.device.write(addr - self.addr, val);
    }

    fn read(&mut self, addr: usize) -> u64 {
        self.device.read(addr - self.addr)
    }

    fn size(&self) -> usize {
        self.device.size()
    }
}

impl Display for IODevice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} @ [0x{:x}:0x{:x}]",
            self.name,
            self.addr,
            self.addr + self.size() - 1
        )
    }
}
