extern crate rand;
use vm::hw::MMIO;
pub struct RNG {}

impl RNG {
    pub fn new() -> RNG {
        RNG {}
    }
}

impl MMIO for RNG {
    fn write(&mut self, addr: usize, val: u64) {}

    fn read(&mut self, addr: usize) -> u64 {
        let ret = match addr {
            0 => rand::random::<u64>(),
            1 => rand::random::<f64>().to_bits(),
            _ => panic!("Invalid read"),
        };
        ret
    }

    fn size(&self) -> usize {
        2
    }
}
