use io::{
    self,
    BufRead,
    Write,
};
use std::collections::VecDeque;
use vm::hw::MMIO;
pub struct TTY {
    out_buffer: VecDeque<u8>,
    in_buffer:  VecDeque<u8>,
}

impl TTY {
    pub fn new() -> TTY {
        TTY {
            out_buffer: VecDeque::new(),
            in_buffer:  VecDeque::new(),
        }
    }

    fn flush(&mut self) {
        while let Some(c) = self.out_buffer.pop_front() {
            print!("{}", char::from(c as u8));
        }
        io::stdout().flush().unwrap();
    }

    fn readline(&mut self) -> u64 {
        let stdin = io::stdin();
        let mut line = Vec::new();
        let ret = match stdin.lock().read_until(0xa, &mut line) {
            Ok(num) => ((num as u64) << 16),
            Err(_) => 0,
        };
        for b in line {
            self.in_buffer.push_back(b);
        }
        debug!("RL: {:x}", ret);
        ret
    }
}

impl MMIO for TTY {
    fn write(&mut self, addr: usize, val: u64) {
        match addr {
            0 => self.out_buffer.push_back((val & 0xff) as u8),
            1 => {
                for c in format!("{}", val).bytes() {
                    self.out_buffer.push_back(c);
                }
            },
            2 => {
                for c in format!("0x{:x}", val).bytes() {
                    self.out_buffer.push_back(c);
                }
            },
            3 => {
                for c in format!("{}", f64::from_bits(val)).bytes() {
                    self.out_buffer.push_back(c);
                }
            },

            4 => self.flush(),
            5 => self.out_buffer.clear(),
            6 => self.in_buffer.clear(),
            _ => (),
        }
    }

    fn read(&mut self, addr: usize) -> u64 {
        let ret = match addr {
            0 => match self.in_buffer.pop_front() {
                Some(val) => u64::from(val),
                None => 0x100u64,
            },
            1 => self.readline(),
            _ => 0x100u64,
        };
        ret
    }

    fn size(&self) -> usize {
        0x10
    }
}
