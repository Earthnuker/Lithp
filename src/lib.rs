#![feature(box_patterns)]
#![feature(try_trait)]
#![feature(slice_patterns)]
#![feature(test)]

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;
#[macro_use]
extern crate structopt;
#[macro_use]
extern crate log;
#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate derivative;

extern crate byteorder;
extern crate colored;
extern crate loggerv;
extern crate minifb;

pub mod arg_parser;
pub mod bytecode;
pub mod compiler;
pub mod eval;
pub mod lexer;
pub mod logger;
pub mod parser;
pub mod vm;

use bytecode::Encodable;
use colored::Colorize;
use eval::{
    Context,
    DataType as DT,
};
use lexer::{
    to_expr,
    tokenize,
    Token,
};
// use optimizer::optimize;
use parser::{
    to_ast,
    AST,
};
use std::io::{
    self,
    BufRead,
    Write,
};

mod version_info {
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

pub fn version() {
    println!(
        "{} v{}{} built for {} with {} by {} on {}",
        version_info::PKG_NAME,
        version_info::PKG_VERSION,
        version_info::GIT_VERSION.map_or_else(|| "".to_owned(), |v| format!(" (git {})", v)),
        version_info::TARGET,
        version_info::RUSTC_VERSION,
        version_info::PKG_AUTHORS,
        version_info::BUILT_TIME_UTC,
    )
}

pub fn build_ast(inp: &str) -> Result<AST, String> {
    info!("Input: {}", inp.cyan());
    let tokens = tokenize(&inp)?;
    info!("Tokens: {}", format!("{:?}", tokens).blue());
    let nested_tokens = to_expr(&mut tokens.clone())?;
    info!("Nested: {}", format!("{:?}", nested_tokens).purple());
    let ast = to_ast(&nested_tokens)?;
    info!("AST: {}", format!("{:?}", ast).yellow());
    Ok(ast)
}

pub fn repl_eval(inp: &str, ctx: &mut Context) {
    let ast = match build_ast(inp) {
        Ok(ast) => ast,
        Err(msg) => {
            error!("{}", msg);
            return;
        },
    };
    compiler::compile(&ast);
    let res = ast.eval(ctx);
    match res {
        DT::Err(msg) => error!("{}", msg),
        other => println!("Result: {}", other),
    }
    debug!("Ctx: {:?}", ctx);
}

fn prompt() {
    print!("lithp> ");
    io::stdout().flush().unwrap();
}

pub fn repl(ctx: &mut Context) {
    prompt();
    let stdin = io::stdin();
    for inp in stdin.lock().lines() {
        if inp.is_err() {
            break;
        }
        let inp = inp.unwrap().trim().to_string();
        if inp == "exit" {
            break;
        };
        repl_eval(&inp, ctx);
        prompt();
    }
}
